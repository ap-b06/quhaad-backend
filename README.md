[![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/master/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/master)[![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/master/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/master)

Developing Quhaad Project!

shafiya/question:   [![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/shafiya/question/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/shafiya/question) [![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/shafiya/question/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/shafiya/question)

amel/statistic:     [![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/amel/statistic/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/amel/statistic)[![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/amel/statistic/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/amel/statistic)

luqman-question:    [![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/luqman-question/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/luqman-question) [![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/luqman-question/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/luqman-question)

ronaldi_scoreboard: [![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/scoreboard_ronaldi/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/scoreboard_ronaldi) [![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/scoreboard_ronaldi/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/scoreboard_ronaldi)

dev_websocket:      [![pipeline status](https://gitlab.com/ap-b06/quhaad-backend/badges/dev_websocket/pipeline.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/dev_websocket)[![coverage report](https://gitlab.com/ap-b06/quhaad-backend/badges/dev_websocket/coverage.svg)](https://gitlab.com/ap-b06/quhaad-backend/-/commits/dev_websocket)