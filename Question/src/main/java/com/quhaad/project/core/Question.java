package com.quhaad.project.core;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.quhaad.project.core.QuestionSet;
import java.util.List;

@Embeddable
public class Question {
    private String questionText;
    private String questionMediaUrl;
    private String correctAnswer;
    private int timeLimit;

    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionMediaUrl() {
        return questionMediaUrl;
    }

    public void setQuestionMediaUrl(String questionMediaUrl) {
        this.questionMediaUrl = questionMediaUrl;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }
}
