package com.quhaad.project.core;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "questionSet")
public class QuestionSet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int questionSetId;

    @Column(name = "questionSetName", nullable = false)
    private String questionSetName;

    @ElementCollection
    @CollectionTable(
            name="question",
            joinColumns=@JoinColumn(name="questionSetId")
    )
    private List<Question> listOfQuestions = new ArrayList<>();

    public int getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(int questionSetId) {
        this.questionSetId = questionSetId;
    }

    public String getQuestionSetName() {
        return questionSetName;
    }

    public void setQuestionSetName(String questionSetName) {
        this.questionSetName = questionSetName;
    }

    public List<Question> getListOfQuestions() {
        return listOfQuestions;
    }

    public void setListOfQuestions(List<Question> listOfQuestions) {
        this.listOfQuestions = listOfQuestions;
    }
}
