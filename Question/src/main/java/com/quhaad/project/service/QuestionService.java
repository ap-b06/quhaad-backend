package com.quhaad.project.service;

import com.quhaad.project.core.QuestionSet;
import java.util.List;
import java.util.Optional;

public interface QuestionService {
    public List<QuestionSet> findAllQuestionSet();
    public Optional<QuestionSet> findQuestionSet(int questionSetId);
    public QuestionSet registerQuestionSet(QuestionSet questionSet);
    public void deleteQuestionSet(int questionSetId);
}