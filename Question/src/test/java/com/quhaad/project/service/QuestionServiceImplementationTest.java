package com.quhaad.project.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.quhaad.project.core.QuestionSet;
import com.quhaad.project.repository.QuestionSetRepository;
import com.quhaad.project.service.QuestionServiceImplementation;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplementationTest {
    @Mock
    private QuestionSetRepository questionSetRepository;

    @InjectMocks
    private QuestionServiceImplementation questionService;

    @Test
    public void whenFindAllQuestionSetIsCalledItShouldCallQuestionSetRepositoryFindAll() {
        questionService.findAllQuestionSet();

        verify(questionSetRepository, times(1)).findAll();
    }

    @Test
    public void whenFindQuestionSetIsCalledItShouldCallQuestionSetRepositoryFindById() {
        questionService.findQuestionSet(1);

        verify(questionSetRepository, times(1)).findById(1);
    }

    @Test
    public void whenDeleteQuestionSetIsCalledItShouldCallQuestionSetRepositoryDeleteById() {
        questionService.deleteQuestionSet(1);

        verify(questionSetRepository, times(1)).deleteById(1);
    }

    @Test
    public void whenRegisterQuestionSetIsCalledItShouldCallQuestionSetRepositorySave() {
        QuestionSet questionSet = new QuestionSet();
        questionService.registerQuestionSet(questionSet);

        verify(questionSetRepository, times(1)).save(questionSet);
    }
}